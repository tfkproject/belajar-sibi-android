package com.example.acer.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;


public class EvaluasiActivity extends AppCompatActivity {
    ImageButton kembali;
    MediaPlayer suaraButton;

    ImageButton pilih;
    ImageButton jw1,jw2,jw3;
    ImageView soal;
    int s,s1,j1,j2,j3;
    int skor=0;
    DataKuis data = new DataKuis();
    int n = data.getJumlahSoal();

    boolean jawabanbenar = true;
    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluasi);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        soal = (ImageView) findViewById(R.id.soal);
        jw1 = (ImageButton)findViewById(R.id.jawaban1);
        jw2 = (ImageButton)findViewById(R.id.jawaban2);
        jw3 = (ImageButton)findViewById(R.id.jawaban3);

        suaraButton = MediaPlayer.create(this, R.raw.button);
        newlevel();

        jw1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isCorrect(j1 ==s);
            }
        });

        jw2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isCorrect(j2 ==s);
            }
        });

        jw3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isCorrect(j3 ==s);
            }
        });

        kembali = (ImageButton) findViewById(R.id.back);
        kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suaraButton.start();
                //tutup activity
                finish();
            }
        });
    }

    public void newlevel(){
        s = data.getRandomSoal();
        s1 = data.getRandomSoal();
        int i = new Random().nextInt(3) + 1;

        if(i==1){
            j1 = s;
        }
        else{
            j1 = data.getRandomSoal();
        }

        if(i ==2){
            j2 = s;
        }
        else {
            j2 = data.getRandomSoal();
        }
        if(i ==3){
            j3 = s;
        }
        else{
            j3 = data.getRandomSoal();
        }

        soal.setBackgroundResource(data.getImageSoal(s));
        jw1.setBackgroundResource(data.getImageJawaban(j1));
        jw2.setBackgroundResource(data.getImageJawaban(j2));
        jw3.setBackgroundResource(data.getImageJawaban(j3));
    }

    public void isCorrect(boolean input){
        //TextView tanya =(TextView) findViewById(R.id.pertanyaan);

        if(input && i <n){
            MediaPlayer benar = MediaPlayer.create(this,R.raw.benar);
            skor +=10;
            benar.start();
            newlevel();
            i++;
        }else{
            MediaPlayer salah = MediaPlayer.create(this,R.raw.ya_salah);
            skor -=5;
            salah.start();
        }
        if(input && i >= n){
            AlertDialog.Builder builder = new AlertDialog.Builder(EvaluasiActivity.this);
            builder.setCancelable(false);
            builder.setTitle("Kuis telah selesai.");
            builder.setMessage("Skor anda: "+skor);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }

        //tanya.setText("SKOR : "+ skor);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        suaraButton.stop();
        suaraButton.release();
    }
}
