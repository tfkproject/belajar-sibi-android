package com.example.acer.myapplication;

import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class HelpActivity extends AppCompatActivity {
    TextView txtJudul, txtHalaman, txtStep;
    Button btnPrev, btnNext;
    ImageView img1, img2;

    List<DataHelp> listHelp;
    JSONArray help;
    boolean cekHelp = false;
    int urutanHelp = 0;

    ImageButton kembali;
    MediaPlayer suaraButton;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        listHelp = new ArrayList<DataHelp>();

        suaraButton = MediaPlayer.create(this, R.raw.button);

        img1 = (ImageView) findViewById(R.id.imageView1);
        img2 = (ImageView) findViewById(R.id.imageView2);

        txtJudul = (TextView)  findViewById(R.id.txt_judul);
        txtStep = (TextView)  findViewById(R.id.txt_step);
        txtHalaman = (TextView)  findViewById(R.id.txt_halaman);

        btnNext = (Button) findViewById(R.id.btn_next);
        btnPrev = (Button) findViewById(R.id.btn_prev);

        kembali = (ImageButton) findViewById(R.id.back);
        kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suaraButton.start();
                //tutup activity
                finish();
            }
        });

        btnPrev.setEnabled(false);

        new dapatkanHelp().execute();

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                urutanHelp--;
                if (urutanHelp < 0){
                    urutanHelp = 0;
                }

                btnNext.setEnabled(true);

                if (urutanHelp == 0){
                    btnPrev.setEnabled(false);
                }

                tampilkanHelp(urutanHelp);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                urutanHelp++;
                if (urutanHelp >= listHelp.size()){
                    urutanHelp = listHelp.size() - 1;
                }

                btnPrev.setEnabled(true);

                if (urutanHelp == listHelp.size() - 1){
                    btnNext.setEnabled(false);
                }

                tampilkanHelp(urutanHelp);
            }
        });
    }

    private class dapatkanHelp extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // tampilkan progress dialog
            pDialog = new ProgressDialog(HelpActivity.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            //data materi
            String dtHelp = getResources().getString(R.string.help);

            if(dtHelp != null){
                try{
                    JSONObject jsonObject = new JSONObject(dtHelp);
                    help = jsonObject.getJSONArray("data_help");
                    DataHelp m = null;

                    for(int i = 0; i < help.length(); i++){
                        JSONObject c = help.getJSONObject(i);

                        m = new DataHelp();

                        String judulnya = c.getString("judul");
                        String halamannya = c.getString("halaman");
                        String stepnya = c.getString("step");
                        String gambar1 = c.getString("gambar1");
                        String gambar2 = c.getString("gambar2");

                        m.setJudul(judulnya);
                        m.setHalaman(halamannya);
                        m.setStep(stepnya);
                        m.setImg1(gambar1);
                        m.setImg2(gambar2);
                        listHelp.add(m);
                    }
                } catch (JSONException e){
                    e.printStackTrace();
                }
            }
            else{
                Toast.makeText(HelpActivity.this, "Tidak dapat memuat data atau data materi tidak ada!", Toast.LENGTH_SHORT).show();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //dismis progress dialog
            if(pDialog.isShowing()){
                pDialog.dismiss();
            }

            tampilkanHelp(0);
        }
    }

    private void tampilkanHelp(int urutan_step){
        try{
            DataHelp help = new DataHelp();
            help = listHelp.get(urutan_step);

            //set gambar1
            String gambar1 = help.getImg1();
            int resource1 = getResources().getIdentifier(gambar1,"drawable", getPackageName());
            Glide.with(HelpActivity.this).load(resource1).into(img1);
            //set gambar2
            String gambar2 = help.getImg2();
            int resource2 = getResources().getIdentifier(gambar2,"drawable", getPackageName());
            Glide.with(HelpActivity.this).load(resource2).into(img2);

            //set judul
            String judulnya = help.getJudul();
            txtJudul.setText(judulnya);

            //set step
            String stepnya = help.getStep();
            txtStep.setText(stepnya);


            //set halaman
            txtHalaman.setText("Halaman: "+(urutanHelp + 1)+ " dari " + listHelp.size());

        } catch (Exception e){
            Log.e(this.getClass().toString(), e.getMessage(), e.getCause());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        suaraButton.stop();
        suaraButton.release();
    }
}