package com.example.acer.myapplication;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;

import static com.example.acer.myapplication.R.id.tampil_angka;


public class AngkaActivity extends AppCompatActivity {
    ImageView TampilGambar;
    ImageButton show,hide,pindah;
    MediaPlayer suaraButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_angka);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        suaraButton = MediaPlayer.create(this, R.raw.button);

        TampilGambar = (ImageView) findViewById(tampil_angka);
        //tampilkan gambar awal
        TampilGambar.setImageResource(R.drawable.sb1);

        final Animation animScale = AnimationUtils.loadAnimation(this,R.anim.anim_scale);


        //Untuk Menambahkan Button Suara
        final MediaPlayer Suarasatu = MediaPlayer.create(this, R.raw.satu); //Memanggil nama lagu
        final MediaPlayer Suaradua = MediaPlayer.create(this, R.raw.dua);
        final MediaPlayer Suaratiga = MediaPlayer.create(this, R.raw.tiga);
        final MediaPlayer Suaraempat = MediaPlayer.create(this, R.raw.empat);
        final MediaPlayer Suaralima = MediaPlayer.create(this, R.raw.lima);
        final MediaPlayer Suaraenam = MediaPlayer.create(this, R.raw.enam);
        final MediaPlayer Suaratujuh = MediaPlayer.create(this, R.raw.tujuh);
        final MediaPlayer SuaraDelapan = MediaPlayer.create(this, R.raw.delapan);
        final MediaPlayer Suarasembilan = MediaPlayer.create(this, R.raw.sembilan);
        final MediaPlayer Suarasepuluh = MediaPlayer.create(this, R.raw.sepuluh);


        ImageButton ButtonSuara = (ImageButton) this.findViewById(R.id.satu); //fariabel button
        ImageButton ButtonSuara2 = (ImageButton) this.findViewById(R.id.dua);
        ImageButton ButtonSuara3 = (ImageButton) this.findViewById(R.id.tiga);
        ImageButton ButtonSuara4 = (ImageButton) this.findViewById(R.id.empat);
        ImageButton ButtonSuara5 = (ImageButton) this.findViewById(R.id.lima);
        ImageButton ButtonSuara6 = (ImageButton) this.findViewById(R.id.enam);
        ImageButton ButtonSuara7 = (ImageButton) this.findViewById(R.id.tujuh);
        ImageButton ButtonSuara8 = (ImageButton) this.findViewById(R.id.delapan);
        ImageButton ButtonSuara9 = (ImageButton) this.findViewById(R.id.sembilan);
        ImageButton ButtonSuara10 = (ImageButton) this.findViewById(R.id.sepuluh);

        /*Menghidupkan Suara */
        ButtonSuara.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sb1);
                TampilGambar.startAnimation(animScale);
                Suarasatu.start();
            }
        });

        ButtonSuara2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sb2);
                TampilGambar.startAnimation(animScale);
                Suaradua.start();
            }
        });

        ButtonSuara3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sb3);
                TampilGambar.startAnimation(animScale);
                Suaratiga.start();
            }
        });

        ButtonSuara4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sb4);
                TampilGambar.startAnimation(animScale);
                Suaraempat.start();
            }
        });
        ButtonSuara5.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sb5);
                TampilGambar.startAnimation(animScale);
                Suaralima.start();
            }
        });
        ButtonSuara6.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sb6);
                TampilGambar.startAnimation(animScale);
                Suaraenam.start();
            }
        });
        ButtonSuara7.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sb7);
                TampilGambar.startAnimation(animScale);
                Suaratujuh.start();
            }
        });

        ButtonSuara8.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sb8);
                TampilGambar.startAnimation(animScale);
                SuaraDelapan.start();
            }
        });

        ButtonSuara9.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sb9);
                TampilGambar.startAnimation(animScale);
                Suarasembilan.start();
            }
        });
        ButtonSuara10.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sb10);
                TampilGambar.startAnimation(animScale);
                Suarasepuluh.start();
            }
        });




        pindah = (ImageButton) findViewById(R.id.tandatanya);
        pindah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suaraButton.start();
                Intent intent = new Intent(AngkaActivity.this,fullsibiangkaActivity.class);
                startActivity(intent);
            }
        });

        pindah = (ImageButton) findViewById(R.id.back);
        pindah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suaraButton.start();
                //tutup activity
                finish();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        suaraButton.stop();
        suaraButton.release();
    }
}
