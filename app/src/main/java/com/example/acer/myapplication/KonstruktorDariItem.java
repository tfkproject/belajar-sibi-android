package com.example.acer.myapplication;

/**
 * Created by apple on 2/23/18.
 */

public class KonstruktorDariItem {

    private String kosakata;
    private int gambarnya;

    public KonstruktorDariItem(String kosakata, int gambarnya) {
        this.kosakata = kosakata;
        this.gambarnya = gambarnya;
    }

    public void setKosakata(String kosakata) {
        this.kosakata = kosakata;
    }

    public String getKosakata() {
        return kosakata;
    }

    public void setGambarnya(int gambarnya) {
        this.gambarnya = gambarnya;
    }

    public int getGambarnya() {
        return gambarnya;
    }
}
