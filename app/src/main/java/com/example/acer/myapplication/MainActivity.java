package com.example.acer.myapplication;

import android.app.KeyguardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {
        ImageButton pindah, tmbolMusik;

    private final static int MAX_VOLUME = 100;

    MediaPlayer mp, suaraButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        suaraButton = MediaPlayer.create(this, R.raw.button);

        mp = MediaPlayer.create(this,R.raw.backsound);
        mp.start(); //memutar musik
        mp.setLooping(true);
        //set volume
        int soundVolume = 70;
        float volume = (float) (1 - (Math.log(MAX_VOLUME - soundVolume) / Math.log(MAX_VOLUME)));
        mp.setVolume(volume, volume);


        pindah = (ImageButton) findViewById(R.id.buttonBelajar);
        pindah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suaraButton.start();
                Intent intent = new Intent(MainActivity.this,BelajarActivity.class);
                startActivity(intent);
            }
        });

        pindah = (ImageButton) findViewById(R.id.buttonKuis);
        pindah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suaraButton.start();
                Intent intent = new Intent(MainActivity.this,EvaluasiActivity.class);
                startActivity(intent);
            }
        });

        pindah = (ImageButton) findViewById(R.id.buttonAbout);
        pindah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suaraButton.start();
                Intent intent = new Intent(MainActivity.this,AboutActivity.class);
                startActivity(intent);
            }
        });

        pindah = (ImageButton) findViewById(R.id.buttonHelp);
        pindah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suaraButton.start();
                Intent intent = new Intent(MainActivity.this,HelpActivity.class);
                startActivity(intent);
            }
        });

        tmbolMusik = (ImageButton) findViewById(R.id.buttonMusik);
        tmbolMusik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mp.isPlaying()){
                    mp.pause();
                    //ubah gambar tombol jadi ke musik hidup
                    tmbolMusik.setBackgroundResource(R.drawable.musik_on);
                }
                else{
                    mp.start();
                    //ubah gambar tombol jadi ke musik mati
                    tmbolMusik.setBackgroundResource(R.drawable.musik_off);
                }
            }
        });

        pindah = (ImageButton) findViewById(R.id.buttonExit);
        pindah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suaraButton.start();
                pesan();
            }
        });

    }

    private void pesan(){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setCancelable(false);
        builder.setTitle("Perhatian!");
        builder.setMessage("Yakin ingin keluar?");
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //berhentikan musik
                mp.stop();
                //tutup activity
                finish();
            }
        });

        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /*keluar aplikasi saat tombol back di tekan*/
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        pesan();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        suaraButton.stop();
        mp.stop();

        suaraButton.release();
        mp.release();
    }
}