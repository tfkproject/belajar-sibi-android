package com.example.acer.myapplication;

import android.widget.Toast;

import java.util.Random;

/**
 * Created by apple on 2/22/18.
 */

public class DataKuis {

    private int listsoal[] = {
            //huruf
            R.drawable.kuissbe,
            R.drawable.kuissbh,
            R.drawable.kuissbk,
            R.drawable.kuissbn,
            R.drawable.kuissbp,
            R.drawable.kuissbq,
            R.drawable.kuissbr,
            R.drawable.kuissbx,
            //angka
            R.drawable.kuissb3,
            R.drawable.kuissb6,
            R.drawable.kuissb7,
            R.drawable.kuissb8,
            R.drawable.kuissb9,
            //imbuhan
            R.drawable.kuis_awalanke,
            R.drawable.kuis_awalanme,
            R.drawable.kuis_akhirankan,
            R.drawable.kuis_akhiranman,
            R.drawable.kuis_partikellah,
            R.drawable.kuis_partikelpun,
            R.drawable.kuis_katagantinya,

    };

    private int listjawaban[] = {
            //huruf
            R.drawable.kuise,
            R.drawable.kuish,
            R.drawable.kuisk,
            R.drawable.kuisn,
            R.drawable.kuisp,
            R.drawable.kuisq,
            R.drawable.kuisr,
            R.drawable.kuisx,
            //angka
            R.drawable.kuis3,
            R.drawable.kuis6,
            R.drawable.kuis7,
            R.drawable.kuis8,
            R.drawable.kuis9,
            //imbuhan
            R.drawable.jawaban_ke,
            R.drawable.jawaban_me,
            R.drawable.jawaban_kan,
            R.drawable.jawaban_man,
            R.drawable.jawaban_lah,
            R.drawable.jawaban_pun,
            R.drawable.jawaban_nya,
    };

    public int[] getListSoal(){
        return listsoal;
    }
    public int[] getListjawaban(){
        return listjawaban;
    }

    public int getRandomSoal(){
        int rnd = new Random().nextInt(listsoal.length);
        return rnd;
    }

    public int getImageSoal(int i){
        return listsoal[i];
    }

    public int getImageJawaban(int i){
        return listjawaban[i];
    }

    public int getJumlahSoal(){
        return listsoal.length;
    }
    public int getJumlahJawaban(){
        return listjawaban.length;
    }
}
