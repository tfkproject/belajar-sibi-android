package com.example.acer.myapplication;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;


public class HurufActivity extends AppCompatActivity {
    ImageView TampilGambar;
    ImageButton show,hide,pindah;
    MediaPlayer suaraButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_huruf);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        suaraButton = MediaPlayer.create(this, R.raw.button);

        TampilGambar = (ImageView) findViewById(R.id.tampil_huruf);
        //tampilkan gambar awal
        TampilGambar.setImageResource(R.drawable.sba);

        final Animation animScale = AnimationUtils.loadAnimation(this,R.anim.anim_scale);


        //Untuk Menambahkan Button Suara
        final MediaPlayer Suaraa = MediaPlayer.create(this, R.raw.a); //Memanggil nama lagu
        final MediaPlayer Suarab = MediaPlayer.create(this, R.raw.b);
        final MediaPlayer Suarac = MediaPlayer.create(this, R.raw.c);
        final MediaPlayer Suarad = MediaPlayer.create(this, R.raw.d);
        final MediaPlayer Suarae = MediaPlayer.create(this, R.raw.e);
        final MediaPlayer Suaraf = MediaPlayer.create(this, R.raw.f);
        final MediaPlayer Suarag = MediaPlayer.create(this, R.raw.g);
        final MediaPlayer Suarah = MediaPlayer.create(this, R.raw.h);
        final MediaPlayer Suarai = MediaPlayer.create(this, R.raw.i);
        final MediaPlayer Suaraj = MediaPlayer.create(this, R.raw.j);
        final MediaPlayer Suarak = MediaPlayer.create(this, R.raw.k);
        final MediaPlayer Suaral = MediaPlayer.create(this, R.raw.l);
        final MediaPlayer Suaram = MediaPlayer.create(this, R.raw.m);
        final MediaPlayer Suaran = MediaPlayer.create(this, R.raw.n);
        final MediaPlayer Suarao = MediaPlayer.create(this, R.raw.o);
        final MediaPlayer Suarap = MediaPlayer.create(this, R.raw.p);
        final MediaPlayer Suaraq = MediaPlayer.create(this, R.raw.q);
        final MediaPlayer Suarar = MediaPlayer.create(this, R.raw.r);
        final MediaPlayer Suaras = MediaPlayer.create(this, R.raw.s);
        final MediaPlayer Suarat = MediaPlayer.create(this, R.raw.t);
        final MediaPlayer Suarau = MediaPlayer.create(this, R.raw.u);
        final MediaPlayer Suarav = MediaPlayer.create(this, R.raw.v);
        final MediaPlayer Suaraw = MediaPlayer.create(this, R.raw.w);
        final MediaPlayer Suarax = MediaPlayer.create(this, R.raw.x);
        final MediaPlayer Suaray = MediaPlayer.create(this, R.raw.y);
        final MediaPlayer Suaraz = MediaPlayer.create(this, R.raw.z);


        ImageButton ButtonSuara = (ImageButton) this.findViewById(R.id.a); //fariabel button
        ImageButton ButtonSuara2 = (ImageButton) this.findViewById(R.id.b);
        ImageButton ButtonSuara3 = (ImageButton) this.findViewById(R.id.c);
        ImageButton ButtonSuara4 = (ImageButton) this.findViewById(R.id.d);
        ImageButton ButtonSuara5 = (ImageButton) this.findViewById(R.id.e);
        ImageButton ButtonSuara6 = (ImageButton) this.findViewById(R.id.f);
        ImageButton ButtonSuara7 = (ImageButton) this.findViewById(R.id.g);
        ImageButton ButtonSuara8 = (ImageButton) this.findViewById(R.id.h);
        ImageButton ButtonSuara9 = (ImageButton) this.findViewById(R.id.i);
        ImageButton ButtonSuara10 = (ImageButton) this.findViewById(R.id.j);
        ImageButton ButtonSuara11 = (ImageButton) this.findViewById(R.id.k);
        ImageButton ButtonSuara12 = (ImageButton) this.findViewById(R.id.l);
        ImageButton ButtonSuara13 = (ImageButton) this.findViewById(R.id.m);
        ImageButton ButtonSuara14 = (ImageButton) this.findViewById(R.id.n);
        ImageButton ButtonSuara15 = (ImageButton) this.findViewById(R.id.o);
        ImageButton ButtonSuara16 = (ImageButton) this.findViewById(R.id.p);
        ImageButton ButtonSuara17 = (ImageButton) this.findViewById(R.id.q);
        ImageButton ButtonSuara18 = (ImageButton) this.findViewById(R.id.r);
        ImageButton ButtonSuara19 = (ImageButton) this.findViewById(R.id.s);
        ImageButton ButtonSuara20 = (ImageButton) this.findViewById(R.id.t);
        ImageButton ButtonSuara21 = (ImageButton) this.findViewById(R.id.u);
        ImageButton ButtonSuara22 = (ImageButton) this.findViewById(R.id.v);
        ImageButton ButtonSuara23 = (ImageButton) this.findViewById(R.id.w);
        ImageButton ButtonSuara24 = (ImageButton) this.findViewById(R.id.x);
        ImageButton ButtonSuara25 = (ImageButton) this.findViewById(R.id.y);
        ImageButton ButtonSuara26 = (ImageButton) this.findViewById(R.id.z);


        /*Menghidupkan Suara */
        ButtonSuara.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sba);
                TampilGambar.startAnimation(animScale);
                Suaraa.start();
            }
        });

        ButtonSuara2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbbb);
                TampilGambar.startAnimation(animScale);
                Suarab.start();
            }
        });

        ButtonSuara3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbcc);
                TampilGambar.startAnimation(animScale);
                Suarac.start();
            }
        });

        ButtonSuara4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbdd);
                TampilGambar.startAnimation(animScale);
                Suarad.start();
            }
        });
        ButtonSuara5.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbee);
                TampilGambar.startAnimation(animScale);
                Suarae.start();
            }
        });
        ButtonSuara6.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbff);
                TampilGambar.startAnimation(animScale);
                Suaraf.start();
            }
        });
        ButtonSuara7.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbgg);
                TampilGambar.startAnimation(animScale);
                Suarag.start();
            }
        });

        ButtonSuara8.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbhh);
                TampilGambar.startAnimation(animScale);
                Suarah.start();
            }
        });

        ButtonSuara9.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbii);
                TampilGambar.startAnimation(animScale);
                Suarai.start();
            }
        });
        ButtonSuara10.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbjj);
                TampilGambar.startAnimation(animScale);
                Suaraj.start();
            }
        });

        ButtonSuara11.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbkk);
                TampilGambar.startAnimation(animScale);
                Suarak.start();
            }
        });

        ButtonSuara12.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbll);
                TampilGambar.startAnimation(animScale);
                Suaral.start();
            }
        });

        ButtonSuara13.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbmm);
                TampilGambar.startAnimation(animScale);
                Suaram.start();
            }
        });
        ButtonSuara14.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbnn);
                TampilGambar.startAnimation(animScale);
                Suaran.start();
            }
        });
        ButtonSuara15.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sboo);
                TampilGambar.startAnimation(animScale);
                Suarao.start();
            }
        });
        ButtonSuara16.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbpp);
                TampilGambar.startAnimation(animScale);
                Suarap.start();
            }
        });

        ButtonSuara17.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbqq);
                TampilGambar.startAnimation(animScale);
                Suaraq.start();
            }
        });

        ButtonSuara18.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbrr);
                TampilGambar.startAnimation(animScale);
                Suarar.start();
            }
        });
        ButtonSuara19.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbss);
                TampilGambar.startAnimation(animScale);
                Suaras.start();
            }
        });

        ButtonSuara20.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbtt);
                TampilGambar.startAnimation(animScale);
                Suarat.start();
            }
        });

        ButtonSuara21.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbuu);
                TampilGambar.startAnimation(animScale);
                Suarau.start();
            }
        });

        ButtonSuara22.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbvv);
                TampilGambar.startAnimation(animScale);
                Suarav.start();
            }
        });
        ButtonSuara23.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbww);
                TampilGambar.startAnimation(animScale);
                Suaraw.start();
            }
        });
        ButtonSuara24.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbxx);
                TampilGambar.startAnimation(animScale);
                Suarax.start();
            }
        });
        ButtonSuara25.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbyy);
                TampilGambar.startAnimation(animScale);
                Suaray.start();
            }
        });

        ButtonSuara26.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TampilGambar.setImageResource(R.drawable.sbzz);
                TampilGambar.startAnimation(animScale);
                Suaraz.start();
            }
        });


        pindah = (ImageButton) findViewById(R.id.tandatanya);
        pindah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suaraButton.start();
                Intent intent = new Intent(HurufActivity.this,fullsibihurufActivity.class);
                startActivity(intent);
            }
        });

        pindah = (ImageButton) findViewById(R.id.back);
        pindah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suaraButton.start();
                //tutup activity
                finish();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        suaraButton.stop();
        suaraButton.release();
    }
}