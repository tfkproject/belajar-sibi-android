package com.example.acer.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

public class ImbuhanActivity extends AppCompatActivity {
    //ImageView TampilGambar;
    SubsamplingScaleImageView TampilGambar;
    ImageButton show,hide,pindah;
    MediaPlayer suaraButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imbuhan);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        suaraButton = MediaPlayer.create(this, R.raw.button);

        //TampilGambar = (ImageView) findViewById(R.id.tampil_gambarawalan);
        TampilGambar = (SubsamplingScaleImageView) findViewById(R.id.tampil_gambarawalan);

        //tampilkan gambar awal
        //TampilGambar.setImageResource(R.drawable.awalan_ber);
        TampilGambar.setImage(ImageSource.resource(R.drawable.awalan_ber));

        final Animation animScale = AnimationUtils.loadAnimation(this,R.anim.anim_scale);


        //Untuk Menambahkan Button Suara
        final MediaPlayer Suaraawalanber = MediaPlayer.create(this, R.raw.awalan_ber); //Memanggil nama lagu
        final MediaPlayer Suaraawalandi = MediaPlayer.create(this, R.raw.awalan_di);
        final MediaPlayer Suaraawalanke = MediaPlayer.create(this, R.raw.awalan_ke);
        final MediaPlayer Suaraawalanme = MediaPlayer.create(this, R.raw.awalan_me);
        final MediaPlayer Suaraawalanpe = MediaPlayer.create(this, R.raw.awalan_pe);
        final MediaPlayer Suaraawalanse = MediaPlayer.create(this, R.raw.awalan_se);
        final MediaPlayer Suaraawalanter = MediaPlayer.create(this, R.raw.awalan_ter);
        final MediaPlayer Suaraakhiranan = MediaPlayer.create(this, R.raw.akhiran_an);
        final MediaPlayer Suaraakhirani = MediaPlayer.create(this, R.raw.akhiran_i);
        final MediaPlayer Suaraakhirankan = MediaPlayer.create(this, R.raw.akhiran_kan);
        final MediaPlayer Suaraakhiranman = MediaPlayer.create(this, R.raw.akhiran_man);
        final MediaPlayer Suaraakhiranwan = MediaPlayer.create(this, R.raw.akhiran_wan);
        final MediaPlayer Suaraakhiranwati = MediaPlayer.create(this, R.raw.akhiran_wati);
        final MediaPlayer Suarapartikelkah = MediaPlayer.create(this, R.raw.partikel_kah);
        final MediaPlayer Suarapartikellah = MediaPlayer.create(this, R.raw.partikel_lah);
        final MediaPlayer Suarapartikelpun = MediaPlayer.create(this, R.raw.partikel_pun);
        final MediaPlayer Suarakatagantinya = MediaPlayer.create(this, R.raw.kataganti_nya);


        ImageButton ButtonSuara = (ImageButton) this.findViewById(R.id.gambar_awalanber); //fariabel button
        ImageButton ButtonSuara2 = (ImageButton) this.findViewById(R.id.gambar_awalandi);
        ImageButton ButtonSuara3 = (ImageButton) this.findViewById(R.id.gambar_awalanke);
        ImageButton ButtonSuara4 = (ImageButton) this.findViewById(R.id.gambar_awalanme);
        ImageButton ButtonSuara5 = (ImageButton) this.findViewById(R.id.gambar_awalanpe);
        ImageButton ButtonSuara6 = (ImageButton) this.findViewById(R.id.gambar_awalanse);
        ImageButton ButtonSuara7 = (ImageButton) this.findViewById(R.id.gambar_awalanter);
        ImageButton ButtonSuara8 = (ImageButton) this.findViewById(R.id.gambar_akhiranan);
        ImageButton ButtonSuara9 = (ImageButton) this.findViewById(R.id.gambar_akhirani);
        ImageButton ButtonSuara10 = (ImageButton) this.findViewById(R.id.gambar_akhirankan);
        ImageButton ButtonSuara11 = (ImageButton) this.findViewById(R.id.gambar_akhiranman);
        ImageButton ButtonSuara12 = (ImageButton) this.findViewById(R.id.gambar_akhiranwan);
        ImageButton ButtonSuara13 = (ImageButton) this.findViewById(R.id.gambar_akhiranwati);
        ImageButton ButtonSuara14 = (ImageButton) this.findViewById(R.id.gambar_partikelkah);
        ImageButton ButtonSuara15 = (ImageButton) this.findViewById(R.id.gambar_partikellah);
        ImageButton ButtonSuara16 = (ImageButton) this.findViewById(R.id.gambar_partikelpun);
        ImageButton ButtonSuara17 = (ImageButton) this.findViewById(R.id.gambar_katagantinya);


        /*Menghidupkan Suara */
        ButtonSuara.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //TampilGambar.setImageResource(R.drawable.awalan_ber);
                TampilGambar.setImage(ImageSource.resource(R.drawable.awalan_ber));
                TampilGambar.startAnimation(animScale);
                Suaraawalanber.start();
            }
        });

        ButtonSuara2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //TampilGambar.setImageResource(R.drawable.awalan_di);
                TampilGambar.setImage(ImageSource.resource(R.drawable.awalan_di));
                TampilGambar.startAnimation(animScale);
                Suaraawalandi.start();
            }
        });

        ButtonSuara3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //TampilGambar.setImageResource(R.drawable.awalan_ke);
                TampilGambar.setImage(ImageSource.resource(R.drawable.awalan_ke));
                TampilGambar.startAnimation(animScale);
                Suaraawalanke.start();
            }
        });

        ButtonSuara4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //TampilGambar.setImageResource(R.drawable.awalan_me);
                TampilGambar.setImage(ImageSource.resource(R.drawable.awalan_me));
                TampilGambar.startAnimation(animScale);
                Suaraawalanme.start();

            }
        });
        ButtonSuara5.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //TampilGambar.setImageResource(R.drawable.awalan_pe);
                TampilGambar.setImage(ImageSource.resource(R.drawable.awalan_pe));
                TampilGambar.startAnimation(animScale);
                Suaraawalanpe.start();
            }
        });
        ButtonSuara6.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //TampilGambar.setImageResource(R.drawable.awalan_se);
                TampilGambar.setImage(ImageSource.resource(R.drawable.awalan_se));
                TampilGambar.startAnimation(animScale);
                Suaraawalanse.start();
            }
        });
        ButtonSuara7.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //TampilGambar.setImageResource(R.drawable.awalan_ter);
                TampilGambar.setImage(ImageSource.resource(R.drawable.awalan_ter));
                TampilGambar.startAnimation(animScale);
                Suaraawalanter.start();
            }
        });

        ButtonSuara8.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //TampilGambar.setImageResource(R.drawable.akhiran_an);
                TampilGambar.setImage(ImageSource.resource(R.drawable.akhiran_an));
                TampilGambar.startAnimation(animScale);
                Suaraakhiranan.start();
            }
        });

        ButtonSuara9.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //TampilGambar.setImageResource(R.drawable.akhiran_i);
                TampilGambar.setImage(ImageSource.resource(R.drawable.akhiran_i));
                TampilGambar.startAnimation(animScale);
                Suaraakhirani.start();
            }
        });

        ButtonSuara10.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //TampilGambar.setImageResource(R.drawable.akhiran_kan);
                TampilGambar.setImage(ImageSource.resource(R.drawable.akhiran_kan));
                TampilGambar.startAnimation(animScale);
                Suaraakhirankan.start();

            }
        });
        ButtonSuara11.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //TampilGambar.setImageResource(R.drawable.akhiran_man);
                TampilGambar.setImage(ImageSource.resource(R.drawable.akhiran_man));
                TampilGambar.startAnimation(animScale);
                Suaraakhiranman.start();
            }
        });
        ButtonSuara12.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //TampilGambar.setImageResource(R.drawable.akhiran_wan);
                TampilGambar.setImage(ImageSource.resource(R.drawable.akhiran_wan));
                TampilGambar.startAnimation(animScale);
                Suaraakhiranwan.start();
            }
        });
        ButtonSuara13.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //TampilGambar.setImageResource(R.drawable.akhiran_wati);
                TampilGambar.setImage(ImageSource.resource(R.drawable.akhiran_wati));
                TampilGambar.startAnimation(animScale);
                Suaraakhiranwati.start();
            }
        });
        ButtonSuara14.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //TampilGambar.setImageResource(R.drawable.partikel_kah);
                TampilGambar.setImage(ImageSource.resource(R.drawable.partikel_kah));
                TampilGambar.startAnimation(animScale);
                Suarapartikelkah.start();

            }
        });
        ButtonSuara15.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //TampilGambar.setImageResource(R.drawable.partikel_lah);
                TampilGambar.setImage(ImageSource.resource(R.drawable.partikel_lah));
                TampilGambar.startAnimation(animScale);
                Suarapartikellah.start();
            }
        });
        ButtonSuara16.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //TampilGambar.setImageResource(R.drawable.partikel_pun);
                TampilGambar.setImage(ImageSource.resource(R.drawable.partikel_pun));
                TampilGambar.startAnimation(animScale);
                Suarapartikelpun.start();
            }
        });
        ButtonSuara17.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //TampilGambar.setImageResource(R.drawable.kataganti_nya);
                TampilGambar.setImage(ImageSource.resource(R.drawable.kataganti_nya));
                TampilGambar.startAnimation(animScale);
                Suarakatagantinya.start();
            }
        });

        pindah = (ImageButton) findViewById(R.id.back);
        pindah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suaraButton.start();
                //tutup activity
                finish();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        suaraButton.stop();
        suaraButton.release();
    }

}
