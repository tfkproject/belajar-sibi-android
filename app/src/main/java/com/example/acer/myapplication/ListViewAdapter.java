package com.example.acer.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by apple on 2/23/18.
 */

public class ListViewAdapter extends BaseAdapter {

    // Declare Variables

    Context mContext;
    LayoutInflater inflater;
    private List<KonstruktorDariItem> itemList = null;
    private ArrayList<KonstruktorDariItem> arraylist;

    public ListViewAdapter(Context context, List<KonstruktorDariItem> animalNamesList) {
        mContext = context;
        this.itemList = animalNamesList;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<KonstruktorDariItem>();
        this.arraylist.addAll(animalNamesList);
    }

    public class ViewHolder {
        TextView tv_kosakata;
        ImageView iv_gambar;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public KonstruktorDariItem getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.list_view_items, null);

            holder.tv_kosakata = (TextView) view.findViewById(R.id.kosakata);
            holder.iv_gambar = (ImageView) view.findViewById(R.id.gambar_kosakata);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results
        holder.tv_kosakata.setText(itemList.get(position).getKosakata());
        holder.iv_gambar.setImageResource(itemList.get(position).getGambarnya());
        return view;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        itemList.clear();
        if (charText.length() == 0) {
            itemList.addAll(arraylist);
        } else {
            for (KonstruktorDariItem wp : arraylist) {
                if (wp.getKosakata().toLowerCase(Locale.getDefault()).contains(charText)) {
                    itemList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
