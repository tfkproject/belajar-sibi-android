package com.example.acer.myapplication;

/**
 * Created by taufik on 08/04/18.
 */

public class DataHelp {
    private String judul, halaman, step, img1, img2;

    public DataHelp() {

    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public void setHalaman(String halaman) {
        this.halaman = halaman;
    }

    public String getHalaman() {
        return halaman;
    }

    public void setImg1(String img1) {
        this.img1 = img1;
    }

    public String getImg1() {
        return img1;
    }

    public void setImg2(String img2) {
        this.img2 = img2;
    }

    public String getImg2() {
        return img2;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getStep() {
        return step;
    }
}
