package com.example.acer.myapplication;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.ArrayList;
import java.util.List;


public class KosakataActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    ListView list;
    ListViewAdapter adapter;
    SearchView editsearch;
    List<KonstruktorDariItem> items;
    ImageButton tmblkembali;
    MediaPlayer suaraButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kosakata);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        suaraButton = MediaPlayer.create(this, R.raw.button);

        tmblkembali = (ImageButton) findViewById(R.id.buttonback);
        tmblkembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suaraButton.start();
                //tutup activity
                finish();
            }
        });

        editsearch = (SearchView) findViewById(R.id.search);
        editsearch.clearFocus();

        list = (ListView) findViewById(R.id.listview);

        items = new ArrayList<>();
        items.add(new KonstruktorDariItem("abad", R.drawable.abad));
        items.add(new KonstruktorDariItem("abu-abu", R.drawable.abu_abu));
        items.add(new KonstruktorDariItem("abjad", R.drawable.abjad));
        items.add(new KonstruktorDariItem("adil", R.drawable.adil));
        items.add(new KonstruktorDariItem("adu", R.drawable.adu));
        items.add(new KonstruktorDariItem("aduk", R.drawable.aduk));
        items.add(new KonstruktorDariItem("agak", R.drawable.agak));
        items.add(new KonstruktorDariItem("agama", R.drawable.agama));
        items.add(new KonstruktorDariItem("agar", R.drawable.agar));
        items.add(new KonstruktorDariItem("ahli", R.drawable.ahli));
        items.add(new KonstruktorDariItem("aib", R.drawable.aib));
        items.add(new KonstruktorDariItem("air", R.drawable.air));
        items.add(new KonstruktorDariItem("baca", R.drawable.baca));
        items.add(new KonstruktorDariItem("bahas", R.drawable.bahas));
        items.add(new KonstruktorDariItem("bakti", R.drawable.bakti));
        items.add(new KonstruktorDariItem("bakso", R.drawable.bakso));
        items.add(new KonstruktorDariItem("baklik", R.drawable.balik));
        items.add(new KonstruktorDariItem("balon", R.drawable.balon));
        items.add(new KonstruktorDariItem("bangku", R.drawable.bangku));
        items.add(new KonstruktorDariItem("bangsa", R.drawable.bangsa));
        items.add(new KonstruktorDariItem("bangun", R.drawable.bangun));
        items.add(new KonstruktorDariItem("bandel", R.drawable.bandel));
        items.add(new KonstruktorDariItem("bantu", R.drawable.bantu));
        items.add(new KonstruktorDariItem("banyak", R.drawable.banyak));
        items.add(new KonstruktorDariItem("bapak", R.drawable.bapak));
        items.add(new KonstruktorDariItem("baring", R.drawable.baring));
        items.add(new KonstruktorDariItem("baris", R.drawable.baris));
        items.add(new KonstruktorDariItem("baru", R.drawable.baru));
        items.add(new KonstruktorDariItem("basah", R.drawable.basah));
        items.add(new KonstruktorDariItem("cabai", R.drawable.cabai));
        items.add(new KonstruktorDariItem("cabut", R.drawable.cabut));
        items.add(new KonstruktorDariItem("calon", R.drawable.calon));
        items.add(new KonstruktorDariItem("cangkir", R.drawable.cangkir));
        items.add(new KonstruktorDariItem("dada", R.drawable.dada));
        items.add(new KonstruktorDariItem("daerah", R.drawable.daerah));
        items.add(new KonstruktorDariItem("dahulu", R.drawable.dahulu));
        items.add(new KonstruktorDariItem("dalam", R.drawable.dalam));
        items.add(new KonstruktorDariItem("damping", R.drawable.damping));
        items.add(new KonstruktorDariItem("dan", R.drawable.dan));
        items.add(new KonstruktorDariItem("dasar", R.drawable.dasar));
        items.add(new KonstruktorDariItem("eja", R.drawable.eja));
        items.add(new KonstruktorDariItem("elok", R.drawable.elok));
        items.add(new KonstruktorDariItem("emak", R.drawable.emak));
        items.add(new KonstruktorDariItem("enak", R.drawable.enak));
        items.add(new KonstruktorDariItem("erti", R.drawable.erti));
        items.add(new KonstruktorDariItem("esok", R.drawable.esok));
        items.add(new KonstruktorDariItem("famili", R.drawable.famili));
        items.add(new KonstruktorDariItem("gabung", R.drawable.gabung));
        items.add(new KonstruktorDariItem("gagas", R.drawable.gagas));
        items.add(new KonstruktorDariItem("gajah", R.drawable.gajah));
        items.add(new KonstruktorDariItem("gambar", R.drawable.gambar));
        items.add(new KonstruktorDariItem("ganggu", R.drawable.ganggu));
        items.add(new KonstruktorDariItem("ganjil", R.drawable.ganjil));
        items.add(new KonstruktorDariItem("ganti", R.drawable.ganti));
        items.add(new KonstruktorDariItem("garam", R.drawable.garam));
        items.add(new KonstruktorDariItem("habis", R.drawable.habis));
        items.add(new KonstruktorDariItem("hadap", R.drawable.hadap));
        items.add(new KonstruktorDariItem("hadiah", R.drawable.hadiah));
        items.add(new KonstruktorDariItem("hangus", R.drawable.hangus));
        items.add(new KonstruktorDariItem("ibadah", R.drawable.ibadah));
        items.add(new KonstruktorDariItem("ibu", R.drawable.ibu));
        items.add(new KonstruktorDariItem("ide", R.drawable.ide));
        items.add(new KonstruktorDariItem("ikan", R.drawable.ikan));
        items.add(new KonstruktorDariItem("ikat", R.drawable.ikat));
        items.add(new KonstruktorDariItem("ikhlas", R.drawable.ikhlas));
        items.add(new KonstruktorDariItem("ilmu", R.drawable.ilmu));
        items.add(new KonstruktorDariItem("imam", R.drawable.imam));
        items.add(new KonstruktorDariItem("iman", R.drawable.iman));
        items.add(new KonstruktorDariItem("jadi", R.drawable.jadi));
        items.add(new KonstruktorDariItem("jadwal", R.drawable.jadwal));
        items.add(new KonstruktorDariItem("jaga", R.drawable.jaga));
        items.add(new KonstruktorDariItem("jahil", R.drawable.jahil));
        items.add(new KonstruktorDariItem("jalan", R.drawable.jalan));
        items.add(new KonstruktorDariItem("jumat", R.drawable.jumat));
        items.add(new KonstruktorDariItem("kabar", R.drawable.kabar));
        items.add(new KonstruktorDariItem("kaca", R.drawable.kaca));
        items.add(new KonstruktorDariItem("kacamata", R.drawable.kacamata));
        items.add(new KonstruktorDariItem("kakak", R.drawable.kakak));
        items.add(new KonstruktorDariItem("kaleng", R.drawable.kaleng));
        items.add(new KonstruktorDariItem("kali", R.drawable.kali));
        items.add(new KonstruktorDariItem("kalimat", R.drawable.kalimat));
        items.add(new KonstruktorDariItem("kapan", R.drawable.kapan));
        items.add(new KonstruktorDariItem("lahir", R.drawable.lahir));
        items.add(new KonstruktorDariItem("lalu", R.drawable.lalu));
        items.add(new KonstruktorDariItem("lama", R.drawable.lama));
        items.add(new KonstruktorDariItem("lancar", R.drawable.lancar));
        items.add(new KonstruktorDariItem("langsung", R.drawable.langsung));
        items.add(new KonstruktorDariItem("lantai", R.drawable.lantai));
        items.add(new KonstruktorDariItem("lemah", R.drawable.lemah));
        items.add(new KonstruktorDariItem("maaf", R.drawable.maaf));
        items.add(new KonstruktorDariItem("mahal", R.drawable.mahal));
        items.add(new KonstruktorDariItem("mahasiswa", R.drawable.mahasiswa));
        items.add(new KonstruktorDariItem("makan", R.drawable.makan));
        items.add(new KonstruktorDariItem("nenek", R.drawable.nenek));
        items.add(new KonstruktorDariItem("nyaris", R.drawable.nyaris));
        items.add(new KonstruktorDariItem("nyata", R.drawable.nyata));
        items.add(new KonstruktorDariItem("obat", R.drawable.obat));
        items.add(new KonstruktorDariItem("ogah", R.drawable.ogah));
        items.add(new KonstruktorDariItem("oh", R.drawable.oh));
        items.add(new KonstruktorDariItem("paham", R.drawable.paham));
        items.add(new KonstruktorDariItem("pahit", R.drawable.pahit));
        items.add(new KonstruktorDariItem("paragraf", R.drawable.paragraf));
        items.add(new KonstruktorDariItem("parah", R.drawable.parah));
        items.add(new KonstruktorDariItem("rabu", R.drawable.rabu));
        items.add(new KonstruktorDariItem("raksasa", R.drawable.raksasa));
        items.add(new KonstruktorDariItem("rakus", R.drawable.rakus));
        items.add(new KonstruktorDariItem("rakyat", R.drawable.rakyat));
        items.add(new KonstruktorDariItem("rampung", R.drawable.rampung));
        items.add(new KonstruktorDariItem("saku", R.drawable.saku));
        items.add(new KonstruktorDariItem("salin", R.drawable.salin));
        items.add(new KonstruktorDariItem("saling", R.drawable.saling));
        items.add(new KonstruktorDariItem("sambil", R.drawable.sambil));
        items.add(new KonstruktorDariItem("taat", R.drawable.taat));
        items.add(new KonstruktorDariItem("tadi", R.drawable.tadi));
        items.add(new KonstruktorDariItem("tahun", R.drawable.tahun));
        items.add(new KonstruktorDariItem("tajam", R.drawable.tajam));
        items.add(new KonstruktorDariItem("tambah", R.drawable.tambah));
        items.add(new KonstruktorDariItem("ulang", R.drawable.ulang));
        items.add(new KonstruktorDariItem("usia", R.drawable.usia));
        items.add(new KonstruktorDariItem("usil", R.drawable.usil));
        items.add(new KonstruktorDariItem("usir", R.drawable.usir));
        items.add(new KonstruktorDariItem("wajah", R.drawable.wajah));
        items.add(new KonstruktorDariItem("wajib", R.drawable.wajib));
        items.add(new KonstruktorDariItem("wangi", R.drawable.wangi));
        items.add(new KonstruktorDariItem("ya", R.drawable.ya));
        items.add(new KonstruktorDariItem("yaitu", R.drawable.yaitu));
        items.add(new KonstruktorDariItem("yakin", R.drawable.yakin));
        items.add(new KonstruktorDariItem("yakni", R.drawable.yakni));

        adapter = new ListViewAdapter(this, items);

        list.setAdapter(adapter);

        editsearch.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String text = newText;
        adapter.filter(text);
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        suaraButton.stop();
        suaraButton.release();
    }

}
